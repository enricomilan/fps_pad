/**
 * This example turns the ESP32 into a Bluetooth LE keyboard that writes the words, presses Enter, presses a media key and then Ctrl+Alt+Delete
 */
#include <BleKeyboard.h>
//BleKeyboard bleKeyboard("Bluetooth Device Name", "Bluetooth Device Manufacturer", 100); l'ultimo parametro è la percentuale di batteria

BleKeyboard bleKeyboard;
int val = 1;
void setup() {
  Serial.begin(115200);
  Serial.println("Starting BLE work!");
  bleKeyboard.begin();
  pinMode(35, 0);
}

void loop() {
  val = digitalRead(35);   // read the input pin  
    if(val == 0){
      bleKeyboard.press('w');
        Serial.println("premuto");

      }
      else{bleKeyboard.releaseAll();}
    }
  
    /*
    Serial.println("Sending 'Hello world'...");
    bleKeyboard.print("Hello world");

    delay(1000);
    Serial.println("Sending Enter key...");
    bleKeyboard.write(KEY_RETURN);

    delay(1000);

    Serial.println("Sending Play/Pause media key...");
    bleKeyboard.write(KEY_MEDIA_PLAY_PAUSE);

    delay(1000);

    Serial.println("Sending Ctrl+Alt+Delete...");
    bleKeyboard.press(KEY_LEFT_CTRL);
    bleKeyboard.press(KEY_LEFT_ALT);
    bleKeyboard.press(KEY_DELETE);
    delay(100);
    bleKeyboard.releaseAll();
  }

  Serial.println("Waiting 5 seconds...");
  delay(5000);
}*/
