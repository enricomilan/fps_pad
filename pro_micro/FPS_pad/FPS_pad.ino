/*
Scritto da Enrico Milan 
data inizio 05/05/2020
data fine 
Arduino ProMicro.
FPS pad 
pad analogico con tasti pensato 
per sostituire parzialmente l'uso della tastiera nei videogiochi
il software simula una tastiera HID USB utilizzando la libreria keyboard 
*/

#include "Keyboard.h"

//variabili per la funzione debounce
long prev_tempo = 0;
long tempo = 0;
long delta_tempo =0;
int last_stato_button = 1;
unsigned long tim1 = 0;
unsigned long longbutton = 0;

const int toggle_switch = 14; //switch per attivare la funzionalità HID
//dichiaro i pin come variabili
const int button1 = 2; //pulsante 1
const int button2 = 3;
const int button3 = 4;
const int button4 = 5;
const int button5 = 6;
const int button6 = 7;
const int button7 = 10;
const int button8 = 8; 
const int button9 = 9;
const int button10 = 16; //pulsante 10 pulsante analog

const int analogy = A1; //analogico asse x
const int analogx = A0; //analogico asse y

//stato dei vari pulsanti
int hid = 0;
int read_toggle = 1;
int toggle_deb = 1; 
int button1state = 1; //pulsante 1
int button2state = 1;
int button3state = 1;
int button4state = 1;
int button5state = 1;
int button6state = 1;
int button7state = 1;
int button8state = 1; 
int button9state = 1; 
int button10state = 1; 


int analogxval = 0; //valore analogico asse x
int analogyval = 0; //valore analogico asse y


void setup() {
//inizializzo i pin
  pinMode(button1, INPUT_PULLUP);
  pinMode(button2, INPUT_PULLUP);
  pinMode(button3, INPUT_PULLUP);
  pinMode(button4, INPUT_PULLUP);
  pinMode(button5, INPUT_PULLUP);
  pinMode(button6, INPUT_PULLUP);
  pinMode(button7, INPUT_PULLUP);
  pinMode(button8, INPUT_PULLUP);
  pinMode(button9, INPUT_PULLUP);
  pinMode(button10, INPUT_PULLUP);//bottone analogico


  pinMode(toggle_switch, INPUT_PULLUP);
  Serial.begin(9600);
  //Keyboard.begin();//accendo hid

}

     

void loop() {

readall();//leggo tutti gli ingressi
hid_keypad();//eseguo funzione keypad in loop finchè hid è acceso
      
}
/*
int debounce(int stato_button, int deb_delay){//funzione unica per fare debounce di un pulsante
 if (stato_button != last_stato_button){
    prev_tempo = millis();
  }
  tempo = millis();
  delta_tempo = tempo-prev_tempo;
    if (delta_tempo > 2){
      last_stato_button = stato_button;
      return stato_button;
      }  
  
}
*/
void readall(){
  button1state = digitalRead(button1);
  button2state = digitalRead(button2);
  button3state = digitalRead(button3);
  button4state = digitalRead(button4);
  button5state = digitalRead(button5);
  button6state = digitalRead(button6);
  button7state = digitalRead(button7);
  button8state = digitalRead(button8);
  button9state = digitalRead(button9);
  analogxval = analogRead(analogx); //leggo valore analogico asse x
  analogyval = analogRead(analogy); //leggo valore analogico asse y  
  
}

void hid_keypad (){
  
 ///////////////movimento sinistra destra
  if (analogxval < 400){
    Keyboard.press('w');
    Serial.print("w");

    }
  else if (analogxval > 600){
    Keyboard.press('s');
    Serial.print("s");

    } 
  else{
    Keyboard.release('w');
    Keyboard.release('s');
    }
///////////////movimento avanti indietro
  if (analogyval < 300){
    Keyboard.press('a');
    Serial.print("a");

    }
  else if (analogyval > 600){
    Keyboard.press('d');
    Serial.print("d");

    } 
  else{
    Keyboard.release('a');
    Keyboard.release('d');
    } 

//////////////pulsante 1       
  if (button1state == 0){
     Serial.println("tasto 1");
     Keyboard.press('i');
  }
  else {Keyboard.release('i');}

//////////////pulsante 4 + longpress
if (button4state == 0){////////accovacciato long press sdraiato
    
    if (longbutton == 0){
      longbutton = 1;
      Serial.println("tasto 4");
      Keyboard.press('c');
      }
    if (millis() - tim1 >= 500 && longbutton == 1){
      longbutton = 2;
      Serial.println("long tasto 4");
      Keyboard.release('c');
      Keyboard.press(KEY_LEFT_CTRL);
      }
     if (longbutton == 2){Keyboard.release(KEY_LEFT_CTRL);} 
    }
  else {
    longbutton = 0;
    tim1 = millis(); 
    Keyboard.release(KEY_LEFT_CTRL);
    Keyboard.release('c');
    }       


//////////////pulsante 3       
  if (button3state == 0){
     Serial.println("tasto 3");
     Keyboard.press(' ');
  }
  else {Keyboard.release(' ');}

//////////////pulsante  2      
  if (button2state == 0){
     Serial.println("tasto 2");
     Keyboard.press('n');
  }
  else {Keyboard.release('n');} 

//////////////pulsante  5      
  if (button5state == 0){
     Serial.println("tasto 5");
     Keyboard.press('4');
  }
  else {Keyboard.release('4');} 

//////////////pulsante  6      
  if (button6state == 0){
     Serial.println("tasto 6");
     Keyboard.press('m');
  }
  else {Keyboard.release('m');}

//////////////pulsante  7      
  if (button7state == 0){
     Serial.println("tasto 7");
     Keyboard.press('x');
  }
  else {Keyboard.release('x');}

//////////////pulsante  8      
  if (button8state == 0){
     Serial.println("tasto 8");
     Keyboard.press('3');
  }
  else {Keyboard.release('3');}

//////////////pulsante  9      
  if (button9state == 0){
     Serial.println("tasto 9");
     Keyboard.press('e');
  }
  else {Keyboard.release('e');}              

//////////////pulsante  10      
  if (button10state == 0){
     Serial.println("tasto 9");
     Keyboard.press(KEY_LEFT_SHIFT);
  }
  else {Keyboard.release(KEY_LEFT_SHIFT);} 
  
}
